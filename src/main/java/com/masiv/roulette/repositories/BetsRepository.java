package com.masiv.roulette.repositories;
import com.masiv.roulette.models.BetsModel;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
@Repository
public interface BetsRepository extends CrudRepository<BetsModel, Long>{}