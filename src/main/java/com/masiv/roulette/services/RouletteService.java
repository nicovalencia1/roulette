package com.masiv.roulette.services;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import com.masiv.roulette.models.BetsModel;
import com.masiv.roulette.models.RouletteModel;
import com.masiv.roulette.models.RouletteResponseModel;
import com.masiv.roulette.repositories.BetsRepository;
import com.masiv.roulette.repositories.RouletteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
/**
 * Destined logic service for handling roulettes
 */
@Service
public class RouletteService {
    @Autowired
    RouletteRepository rouletteRepository;
    @Autowired
    BetsRepository betsRepository;
    public ArrayList<RouletteModel> getRoulettes() {
        return (ArrayList<RouletteModel>) rouletteRepository.findAll();
    }
    public Long saveRoulettes(RouletteModel roulette) {
        roulette.setStateRoulette(false);

        return rouletteRepository.save(roulette).getIdRoulette();
    }
    public RouletteResponseModel rouletteOpening(RouletteModel roulette) {
        RouletteResponseModel response = new RouletteResponseModel();
        if(rouletteRepository.findById(roulette.getIdRoulette()).isEmpty()){
            response.setStatus(false);
            response.setMessage("Bet declined, roulette does nos exist");
        }else{
            roulette.setStateRoulette(true);
            String time = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime());
            roulette.setOpeningDateRoulette(time);
            rouletteRepository.save(roulette);
            response.setStatus(true);
            response.setIdRoulette(roulette.getIdRoulette());
        }       
        
        return response;
    }
    public ArrayList<BetsModel> rouletteClose(RouletteModel roulette) {
        ArrayList<BetsModel> betsWinners = new ArrayList<>();                
        if(!(rouletteRepository.findById(roulette.getIdRoulette()).isEmpty())){         
            roulette.setStateRoulette(false);
            String time = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime());
            roulette.setCloseDateRoulette(time);
            rouletteRepository.save(roulette);
            betsWinners = getWinners(roulette);
        }       
        
        return betsWinners;
    }
    public ArrayList<BetsModel> getWinners(RouletteModel roulette){
        ArrayList<BetsModel> betsWinners = new ArrayList<>();
        int numberWin = (int) Math.floor(Math.random()*(36-0+1)+0);
        String color = "";
        if(numberWin % 2 == 0){
            color = "rojo";
        }else{
            color = "negro";
        }
        ArrayList<BetsModel> bets = (ArrayList<BetsModel>)  betsRepository.findAll();
        for (int i = 0; i < bets.size(); i++) {
            BetsModel bet = bets.get(i);
            if(bet.getIdRoulette() == roulette.getIdRoulette()){
                if(bet.getNumber()>36){
                    if(bet.getColor().equalsIgnoreCase(color)){
                        bet.setAmountBet((float) (bet.getAmountBet()*1.8));
                        bet.setWinner(true);
                        betsWinners.add(bet);
                    }else{
                        bet.setAmountBet((float) 0);
                    }
                }else{
                    if(bet.getNumber() == numberWin){
                        bet.setAmountBet((float) (bet.getAmountBet()*5));
                        bet.setWinner(true);
                        betsWinners.add(bet);
                    }else{
                        bet.setAmountBet((float) 0);
                    }
                }
            }
        }

        return betsWinners;
    }
}