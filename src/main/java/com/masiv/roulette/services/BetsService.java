package com.masiv.roulette.services;
import com.masiv.roulette.models.BetsModel;
import com.masiv.roulette.models.BetsResponseModel;
import com.masiv.roulette.repositories.BetsRepository;
import com.masiv.roulette.repositories.RouletteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
/**
 * Destined logic service for bets
 */
@Service
public class BetsService {
    @Autowired
    BetsRepository betsRepository;
    @Autowired
    RouletteRepository rouletteRepository;
    public BetsResponseModel makeBet(BetsModel bets){
        BetsResponseModel response = new BetsResponseModel();
        if(rouletteRepository.findById(bets.getIdRoulette()).isEmpty()){
            response.setStatus(false);
            response.setMessage("Bet declined, roulette does nos exist");
        }else{
            boolean state = rouletteRepository.findById(bets.getIdRoulette()).get().getStateRoulette();
            if(!state){
                response.setStatus(false);
                response.setMessage("Bet declined, roulette is closed");
            }else{
                bets = betsRepository.save(bets);
                response.setStatus(true);
                response.setIdBets(bets.getIdBet());
                response.setMessage("bet accepted");
            }
        }
        
        return response;
    }
}