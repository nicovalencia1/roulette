package com.masiv.roulette.models;
/**
 * Model Destined for the answers in the requests of management of bets
 */
public class BetsResponseModel {
    private boolean status;
    private Long idBets;
    private String message;
    public boolean isStatus() {
        return this.status;
    }
    public boolean getStatus() {
        return this.status;
    }
    public void setStatus(boolean status) {
        this.status = status;
    }
    public Long getIdBets() {
        return this.idBets;
    }
    public void setIdBets(Long idBets) {
        this.idBets = idBets;
    }
    public String getMessage() {
        return this.message;
    }
    public void setMessage(String message) {
        this.message = message;
    }
}