package com.masiv.roulette.models;
import javax.persistence.*;
/**
 * Data model for roulette entity
 */
@Entity
@Table(name = "roulette")
public class RouletteModel {     
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(unique = true, nullable = false)
    private Long idRoulette;
    @Column(columnDefinition = "tinyint default 0")
    private Boolean stateRoulette;
    private String openingDateRoulette;  
    private String closeDateRoulette;  
    public Long getIdRoulette() {
        return this.idRoulette;
    }
    public void setIdRoulette(Long idRoulette) {
        this.idRoulette = idRoulette;
    }
    public Boolean isStateRoulette() {
        return this.stateRoulette;
    }
    public Boolean getStateRoulette() {
        return this.stateRoulette;
    }
    public void setStateRoulette(boolean stateRoulette) {
        this.stateRoulette = stateRoulette;
    }
    public String getOpeningDateRoulette() {
        return this.openingDateRoulette;
    }
    public void setOpeningDateRoulette(String openingDateRoulette) {
        this.openingDateRoulette = openingDateRoulette;
    }
    public String getCloseDateRoulette() {
        return this.closeDateRoulette;
    }
    public void setCloseDateRoulette(String closeDateRoulette) {
        this.closeDateRoulette = closeDateRoulette;
    }    
}