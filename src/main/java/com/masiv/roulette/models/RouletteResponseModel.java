package com.masiv.roulette.models;
/**
 * Model destined for the answers in the requests of management of roulettes
 */
public class RouletteResponseModel {    
    private boolean status;
    private Long idRoulette;
    private String message;
    public boolean isStatus() {
        return this.status;
    }
    public boolean getStatus() {
        return this.status;
    }
    public void setStatus(boolean status) {
        this.status = status;
    }
    public Long getIdRoulette() {
        return this.idRoulette;
    }
    public void setIdRoulette(Long idRoulette) {
        this.idRoulette = idRoulette;
    }
    public String getMessage() {
        return this.message;
    }
    public void setMessage(String message) {
        this.message = message;
    }
}