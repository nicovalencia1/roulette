package com.masiv.roulette.models;
import javax.persistence.*;
/**
 * Data model for bets entity
 */
@Entity
@Table(name = "bets")
public class BetsModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(unique = true, nullable = false)
    private Long idBet;
    private int number;
    private String color;
    private Float amountBet;
    private Long idUser;
    private Long idRoulette;
    private String dateBet;
    private boolean winner;
    public Long getIdBet() {
        return this.idBet;
    }
    public void setIdBet(Long idBet) {
        this.idBet = idBet;
    }
    public int getNumber() {
        return this.number;
    }
    public void setNumber(int number) {
        this.number = number;
    }
    public String getColor() {
        return this.color;
    }
    public void setColor(String color) {
        this.color = color;
    }
    public Float getAmountBet() {
        return this.amountBet;
    }
    public void setAmountBet(Float amountBet) {
        this.amountBet = amountBet;
    }
    public Long getIdUser() {
        return this.idUser;
    }
    public void setIdUser(Long idUser) {
        this.idUser = idUser;
    }
    public Long getIdRoulette() {
        return this.idRoulette;
    }
    public void setIdRoulette(Long idRoulette) {
        this.idRoulette = idRoulette;
    }
    public String getDateBet() {
        return this.dateBet;
    }
    public void setDateBet(String dateBet) {
        this.dateBet = dateBet;
    }
    public boolean isWinner() {
        return this.winner;
    }
    public boolean getWinner() {
        return this.winner;
    }
    public void setWinner(boolean winner) {
        this.winner = winner;
    }
}