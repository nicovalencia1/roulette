package com.masiv.roulette.controllers;
import com.masiv.roulette.models.BetsModel;
import com.masiv.roulette.models.BetsResponseModel;
import com.masiv.roulette.services.BetsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
/**
 * Controller destined to received requests for the bets entity
 */
@RestController
@RequestMapping("/bets")
public class BetsController {
    @Autowired
    BetsService betsService;
    @PostMapping("/make_bet")
    public BetsResponseModel makeBet(@RequestBody BetsModel bets, @RequestHeader Long idUser){
        bets.setIdUser(idUser);
        return this.betsService.makeBet(bets);
    }
}