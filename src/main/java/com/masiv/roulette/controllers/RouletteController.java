package com.masiv.roulette.controllers;
import java.util.ArrayList;
import com.masiv.roulette.models.BetsModel;
import com.masiv.roulette.models.RouletteModel;
import com.masiv.roulette.models.RouletteResponseModel;
import com.masiv.roulette.services.RouletteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
/**
 * Controller destined to received requests for the roulette entity
 */
@RestController
@RequestMapping("/roulettes")
public class RouletteController {
    @Autowired
    RouletteService rouletteService;
    @GetMapping()
    public ArrayList<RouletteModel> getRoulettes() {
        return rouletteService.getRoulettes();
    }
    @PostMapping("/new_roulette")
    public Long saveRoulette() {
        RouletteModel roulette = new RouletteModel();

        return this.rouletteService.saveRoulettes(roulette);
    }
    @GetMapping("/roulette_opening")
    public RouletteResponseModel rouletteOpening(@RequestParam("idRoulette") Long idRoulette) {
        RouletteModel roulette = new RouletteModel();
        roulette.setIdRoulette(idRoulette);

        return this.rouletteService.rouletteOpening(roulette);
    }
    @GetMapping("/roulette_close")
    public ArrayList<BetsModel> rouletteClose(@RequestParam("idRoulette") Long idRoulette) {
        RouletteModel roulette = new RouletteModel();
        roulette.setIdRoulette(idRoulette);

        return this.rouletteService.rouletteClose(roulette);
    }
}